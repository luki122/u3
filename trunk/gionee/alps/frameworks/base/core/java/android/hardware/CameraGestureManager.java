/**
 * May 21, 2013
 */

package android.hardware;

import android.app.Activity;
import android.util.Log;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.GestureEvent;
import android.hardware.Camera.GestureEventCallback;
import android.view.OrientationEventListener;
import android.view.SurfaceView;
import android.view.SurfaceHolder;
import android.graphics.SurfaceTexture;

import android.app.Activity;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.ArrayDeque;


/**
 * The CameraGesureManager is used to manager listeners for camera gesture. When enabled the camera 
 * gesture funtionality, the CameraGestureManager will receive the gesture data, which includes
 * X axis, Y axis, Z axis, gesture type and gesture action. Then callback to application which registered
 * to CameraGestureManager acrroding to the gesture type.
 *
 * @version	1.1
 * @author wutangzhi
 * */

public class CameraGestureManager {

	private static final String TAG = "CameraGestureManager";

	public static final int CAMERA_GESTURE_TYPE_OPEN_HAND_PRESENCE = 1 << GestureEvent.GESTURE_EVENT_TYPE_OPEN_HAND_PRESENCE;
	public static final int CAMERA_GESTURE_TYPE_FIST_PRESENCE = 1 << GestureEvent.GESTURE_EVENT_TYPE_FIST_PRESENCE;
	public static final int CAMERA_GESTURE_TYPE_FACE_PRESENCE = 1 << GestureEvent.GESTURE_EVENT_TYPE_FACE_PRESENCE;
	public static final int CAMERA_GESTURE_TYPE_THUMBS_UP_PRESENCE = 1 << GestureEvent.GESTURE_EVENT_TYPE_THUMBS_UP_PRESENCE;
	public static final int CAMERA_GESTURE_TYPE_VSIGN_PRESENCE = 1 << GestureEvent.GESTURE_EVENT_TYPE_VSIGN_PRESENCE;
	
	private static final int DEFAULT_PREVIEW_SIZE_WIDTH = 320;
	private static final int DEFAULT_PREVIEW_SIZE_HEIGHT = 240;

	private static final int DEFAULT_PREVIEW_ROTATION = 270;

	private static final int DEFAULT_PREVIEW_EXPOSURE_LEVEL = 0;

	private static final int SURFACE_TESTURE_NAME = 1 << 8;
    
	private final HashMap<GestureEventCallback, LegacyListener> mLegacyListenersMap = 
		new HashMap<GestureEventCallback, LegacyListener>();

	private Context mContext;
	private Camera mCamera;
	private Parameters mParameters;
	
	private GestureEventListener mGestureEventListener = new GestureEventListener();
	private CameraManagerThread	mCameraManagerThread = new CameraManagerThread();

	private static final int CAMERA_STATE_IDLE 		= 0;
	private static final int CAMERA_STATE_OPENING 	= 1;
	private static final int CAMERA_STATE_OPEN 		= 2;
	private static final int CAMERA_STATE_CLOSING 	= 3;
	private static final int CAMERA_STATE_CLOSE 	= 4;
	
	private int mCameraState = CAMERA_STATE_IDLE;

	private SurfaceTexture mSurfaceTexture = null;

    private int frontId = -1;
    private MyOrientationEventListener mOrientationListener;

	private int mOrientation = OrientationEventListener.ORIENTATION_UNKNOWN;

    private static CameraGestureManager mInstance = null;
    public static synchronized CameraGestureManager getInstance(Context context) {
        if (mInstance == null) {
        	mInstance = new CameraGestureManager(context);
		}
		
        return mInstance;   
    } 

	/**
	* @hide
       * 
       */
	private CameraGestureManager(Context context) {
		mContext = context;
		mCameraState = CAMERA_STATE_IDLE;

		mSurfaceTexture = new SurfaceTexture(SURFACE_TESTURE_NAME);
        mOrientationListener = new MyOrientationEventListener(context);

		mCameraManagerThread.start();
	}

	/**
	* @hide
       * 
       */
	public void registerCameraGestureListener(int gestureType, GestureEventCallback listener) {
    	Log.i(TAG, "registerCameraGestureListener: " + " gestureType = " + gestureType);
        mOrientation = OrientationEventListener.ORIENTATION_UNKNOWN;
        if (listener == null){
            Log.e(TAG, "registerCameraGestureListener listener is null");
            return;
        }
 
		synchronized(mLegacyListenersMap) {
			LegacyListener legacyListener = mLegacyListenersMap.get(listener);
			if (legacyListener == null) {
				legacyListener = new LegacyListener(listener);
				mLegacyListenersMap.put(listener, legacyListener);
			}

			legacyListener.registerGesture(gestureType);

            if (isCameraEnabled()) {
				setGesture(gestureType);   
            } else {
				if (mCameraState == CAMERA_STATE_OPENING) {
					return;
				}

				mCameraState = CAMERA_STATE_OPENING;
				mCameraManagerThread.enableCamera();
			}
		}
	} 

	/**
	* @hide
       * 
       */
	public void unregisterCameraGestureListener(int gestureType, GestureEventCallback listener) {
        synchronized(mLegacyListenersMap) {
			LegacyListener legacyListener = mLegacyListenersMap.get(listener);
			
            Log.i(TAG, "unregisterCameraGestureListener: listener=" + listener + "   legacyListener=" + legacyListener 
            	+ " gestureType = " + gestureType);
            
			if (legacyListener != null) {
				legacyListener.unregisterGesture(gestureType);

				if (!legacyListener.hasGestures()) {
					mLegacyListenersMap.remove(listener);
				}
			}
			
            if(mOrientationListener != null){
                mOrientationListener.disable();
            }

            Log.i(TAG, "mLegacyListenersMap.isEmpty() = " + mLegacyListenersMap.isEmpty()
                + ", isCameraEnabled() = " + isCameraEnabled() + ", legacyListener = " + legacyListener);
			
            if (mLegacyListenersMap.isEmpty() && (mCameraState == CAMERA_STATE_OPEN 
                        || mCameraState == CAMERA_STATE_OPENING)) {
				mCameraState = CAMERA_STATE_CLOSING;
                mCameraManagerThread.disableCamera();
			}
		}
	}

	private boolean isCameraEnabled() {
		boolean ret = false;
		
		synchronized(mLegacyListenersMap) {
			ret = (mCameraState == CAMERA_STATE_OPEN);
		}

		return ret;
	}

	private boolean openCamera() {
		boolean ret = false;
		CameraInfo info = new CameraInfo();

		if (mCamera == null) {
			int num = Camera.getNumberOfCameras();
			if (num > 0) {
				for (int i = 0; i < num; i ++) {
					Camera.getCameraInfo(i, info);
					if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
						frontId = i;
						break;
					}
				}
			}

			if (frontId != -1) {
				int limitCount = 3;
				int failCount = 0;
				
				while (limitCount > 0) {
					try{
						mCamera = Camera.open(frontId);
					}catch (RuntimeException e) {
						Log.e(TAG, "Failed to open camera.");
					}
                
					if (mCamera == null) {
						try {
						    limitCount --;
						    failCount ++;
						    Log.d(TAG, "open camera faild:" + failCount);
							
						    Thread.sleep(300);
						} catch (Exception e) {
						    Log.e(TAG, "sleep interrupt", e);
						}
					} else {
						setParameters();
						startPreview();

						ret = true;
						break;
					}
				}
			}
		}else {
			ret = true;
		}

		return ret;
	}

	private void closeCamera() {
		if (mCamera != null) {
			mParameters.setSceneMode(Parameters.SCENE_MODE_AUTO);
			mCamera.setParameters(mParameters);
			mCamera.setPreviewCallback(null);
			mCamera.stopPreview();
			mCamera.release();
            
			mCamera = null;
			mParameters = null;
		}
	}

	private void setParameters() {
		if (mCamera != null) {
			if (mParameters == null) {
				mParameters = mCamera.getParameters();
			}
			
			// preview size settings
			mParameters.setPreviewSize(DEFAULT_PREVIEW_SIZE_WIDTH, DEFAULT_PREVIEW_SIZE_HEIGHT);

			// preview default rotation setting
			mParameters.setRotation(getOrientation());

			//set gesture scene mode
			mParameters.setSceneMode(Parameters.SCENE_MODE_GESTURE);

			// gesture event callback settings
			mCamera.setGestureEventCallback(mGestureEventListener);

			mCamera.setParameters(mParameters);
			mParameters = mCamera.getParameters();
		}
	}

	private int getOrientation(){
		int rotation = ((Activity)mContext).getWindowManager().getDefaultDisplay().getRotation();

        if(rotation == -1){
            mOrientation = DEFAULT_PREVIEW_ROTATION;
            return mOrientation;
        }

		switch (rotation) {
			case 0: 
				mOrientation = 270;
				break;
			case 1: 
				mOrientation = 0; 
				break;
			case 3:
				mOrientation = 180;
				break;
			default: 
				mOrientation = DEFAULT_PREVIEW_ROTATION;
		}

		return mOrientation;
	}

	private void setGesture(int gestureType) {
		if (mCamera != null && mParameters != null && mParameters.isGestureEventSupported()) {
			if ((gestureType & CAMERA_GESTURE_TYPE_OPEN_HAND_PRESENCE) != 0) {
				mParameters.setGestureEventType(Parameters.GESTURE_EVENT_OPEN_HAND_PRESENCE);
				mCamera.setParameters(mParameters);
			}

			if ((gestureType & CAMERA_GESTURE_TYPE_FIST_PRESENCE) != 0) {
				mParameters.setGestureEventType(Parameters.GESTURE_EVENT_FIST_PRESENCE);
				mCamera.setParameters(mParameters);				
			}

			if ((gestureType & CAMERA_GESTURE_TYPE_FACE_PRESENCE) != 0) {
				mParameters.setGestureEventType(Parameters.GESTURE_EVENT_FACE_PRESENCE);
				mCamera.setParameters(mParameters);
			}

			if ((gestureType & CAMERA_GESTURE_TYPE_THUMBS_UP_PRESENCE) != 0) {
				mParameters.setGestureEventType(Parameters.GESTURE_EVENT_THUMBS_UP_PRESENCE);
				mCamera.setParameters(mParameters);
			}

			if ((gestureType & CAMERA_GESTURE_TYPE_VSIGN_PRESENCE) != 0) {
				mParameters.setGestureEventType(Parameters.GESTURE_EVENT_VSIGN_PRESENCE);
				mCamera.setParameters(mParameters);
			}

			mParameters = mCamera.getParameters();
		}
	}

	private PreviewCallback mPrvCb = new PreviewCallback();
	private final class PreviewCallback implements android.hardware.Camera.PreviewCallback {
		public void onPreviewFrame(byte[] data, Camera camera) {
		}
	}

	private void startPreview() {
		if (mCamera != null && mSurfaceTexture != null) {
            try {
                mCamera.setPreviewTexture(mSurfaceTexture);
            } catch (IOException e) {
            	//TODO
            }

			mCamera.setPreviewCallback(mPrvCb);

			mCamera.startPreview();
		}
	}

	private class GestureEventListener implements GestureEventCallback {
		LegacyListener legacyListener;

		@Override
		public void onGestureEvent(GestureEvent event) {
			synchronized(mLegacyListenersMap) {
				Iterator iterator = mLegacyListenersMap.keySet().iterator();
				
				while (iterator.hasNext()) {
					GestureEventCallback listener = (GestureEventCallback)iterator.next();
					
					legacyListener = mLegacyListenersMap.get(listener);
					if (legacyListener != null) {
						legacyListener.onGestureEvent(event);
					}
				}
			}
		}
	}

	private class MyOrientationEventListener extends OrientationEventListener {
		public MyOrientationEventListener(Context context) {
			super(context);
		}

		@Override
		public void onOrientationChanged(int orientation) {
			if (orientation == ORIENTATION_UNKNOWN || !isCameraEnabled()) {
			  	return;
			}

			android.hardware.Camera.CameraInfo info =
			     new android.hardware.Camera.CameraInfo();

			if (frontId != -1) {
			 	android.hardware.Camera.getCameraInfo(frontId, info);
			}

			orientation = (orientation + 45) / 90 * 90;

			int rotation = 0;
			if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
			  	rotation = (info.orientation - orientation + 360) % 360;
			}
            synchronized(mLegacyListenersMap) {
                if(!isCameraEnabled()){
                    return;
                }
            
    			if(mParameters != null && mCamera != null){
    			  	if (mOrientation == rotation) {
    					return;
    			  	} else {
    					mOrientation = rotation;
    			  	}
    			  	mParameters.setRotation(rotation);
    			  	mCamera.setParameters(mParameters);
    			}
			}
		}
	}

	private class LegacyListener {
		
		private int mGestureType = 0;
		private GestureEventCallback mTarget = null;

		public LegacyListener(GestureEventCallback listener) {
			mTarget = listener;
		}

		public void onGestureEvent(GestureEvent event) {
			if ((mGestureType & (1 << event.getType())) != 0) {
				mTarget.onGestureEvent(event);
			}
		}

		public void registerGesture(int gestureType) {
			mGestureType |= gestureType;
		}

		public void unregisterGesture(int gestureType) {
			mGestureType &= (~gestureType);
		} 

		public boolean hasGestures() {
			return (mGestureType != 0);
		}

		public int getGestures() {
			return mGestureType;
		}
	}

	private class Command {
		private boolean mValue = false;

		Command(boolean value) {
			mValue = value;
		}

		boolean getValue(){
			return mValue;
		}
	}

	private class CameraManagerThread extends Thread {
		
		Queue<Command> mCmdQueue = null;

		CameraManagerThread() {
			mCmdQueue = new ArrayDeque<Command>();
		}
		
		@Override
		public void run() {
			boolean isEnableCamera = false;
			Command cmd = null;
		
			while (true) {
				synchronized(this) {
					if(mCmdQueue.isEmpty()) {
						try {
							wait();
						} catch (java.lang.InterruptedException e) {
							//ignore.
						}
					} 
					
					cmd = mCmdQueue.poll();
					mCmdQueue.clear();

					if (cmd != null) {
						isEnableCamera = cmd.getValue();
					} else {
						continue;
					}
				}

				if (isEnableCamera) {
					if (!isCameraEnabled() && !openCamera()) {
						synchronized(mLegacyListenersMap) {
							mCameraState = CAMERA_STATE_IDLE;
						}

						continue;
					}

					if (mOrientationListener != null){
	                	mOrientationListener.enable();
	            	}

					synchronized(mLegacyListenersMap) {
						LegacyListener legacyListener = null;
						Iterator iterator = null;

						if (mCameraState != CAMERA_STATE_OPENING) {
							continue;
						} else {
							mCameraState = CAMERA_STATE_OPEN;
						}

						iterator = mLegacyListenersMap.keySet().iterator();
						while (iterator.hasNext()) {
							GestureEventCallback listener = (GestureEventCallback)iterator.next();
							legacyListener = mLegacyListenersMap.get(listener);
							
							if (legacyListener != null) {
								setGesture(legacyListener.getGestures());
							}
						}
					}
				} else {
					closeCamera();
					
					synchronized(mLegacyListenersMap) {
						if (mCameraState == CAMERA_STATE_CLOSING) { 
							mCameraState = CAMERA_STATE_CLOSE;
						} else {
							continue;
						}
					}
				}
			} 
		}

		public synchronized boolean enableCamera() {
			boolean ret = true;

			if (mCmdQueue != null) {
				mCmdQueue.clear();

				Command cmd = new Command(true);
				mCmdQueue.add(cmd);
				
				notifyAll();
			}

			return ret;
		}

		public synchronized boolean disableCamera() {
			boolean ret = true;

			if (mCmdQueue != null) {
				mCmdQueue.clear();

				Command  cmd = new Command(false);
				mCmdQueue.add(cmd);
				
				notifyAll();
			}

			return ret;
		}
	} 
}

