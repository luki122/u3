/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for face beauty.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-09-07
 *
 *************************************************************************************/

#define LOG_TAG "ArcSoftFaceBeauty"
#include <android/log.h>
#include <string.h>
 
#include "ArcSoftFaceBeauty.h"

namespace android { 

ArcSoftFaceBeauty::ArcSoftFaceBeauty()
	: mInitialized(false)
	, mParam()
	, mListener(NULL)
	, mArcSoftFaceProcess(NULL)
	, mFaceBeautyLevel(0)
{
	memset(&mParam, 0, sizeof(mParam));
}

ArcSoftFaceBeauty::~ArcSoftFaceBeauty()
{
	
}

int32 
ArcSoftFaceBeauty::init()
{
	MRESULT res = 0; 

	if (mArcSoftFaceProcess == NULL) {
		mArcSoftFaceProcess = ArcSoftFaceProcess::getInstance();
	}
	
	return res;
}

void  
ArcSoftFaceBeauty::deinit()
{

}

int32
ArcSoftFaceBeauty::setCameraListener(GNCameraFeatureListener * listener)
{
	MRESULT res = 0;

	mListener = listener;
	
	return res;
}
int32
ArcSoftFaceBeauty::setFaceBeautyParam(FaceBeautyParam const faceBeautyParam)
{
	MRESULT res = 0;
	
	FaceBeautyLevelParam_t param = faceBeautyParam.faceBeautyLevelParam;

	/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	+  faceBeautyLevel		description
	+	 1 ~ 12			beauty param will be look-up in mFBParamArray table
	+	    0				beauty param will be recommended according to the age and gender
	+ 	   -1				set specific feature value respectively,such as SlenderFace,SkinBright... 
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	if (param.faceBeautyLevel > 0) {
		if (param.faceBeautyLevel > MAX_FB_LEVEL_SUPPORT) {
			PRINTE("Unsupport the level[%d]", param.faceBeautyLevel);
			return 0;
		}
	
		mParam = mFBParamArray[param.faceBeautyLevel - 1];
		
	} else if (param.faceBeautyLevel < 0) {
		memset(&mParam, 0, sizeof(mParam));
		mParam.lSkinSoftenLevel 	= param.skinSoftenLevel;
		mParam.lSkinBrightLevel		= param.skinBrightLevel;
		mParam.lSlenderFaceLevel	= param.slenderFaceLevel;
		mParam.lEyeEnlargmentLevel	= param.eyeEnlargmentLevel;
		mParam.lEyeBrightLevel		= EYE_BRIGHT_VALUE;//param.eyeBrightLevel;
		mParam.lTeethWhiteLevel		= TEETH_WHITE_VALUE;//param.teethWhiteLevel;
		mParam.lRemoveShineLevel	= REMOVE_SHINE_VALUE;//param.removeShineLevel;
		mParam.lSkinRuddyLevel		= SKIN_RUDDY_VALUE;
		mParam.lShapeStrengthLevel	= 0;//SHAPE_STRENGTH_VALUE;
		mParam.lBlushLevel			= BLUSH_VALUE;
		
		PRINTD("%ld, %ld, %ld, %ld, %ld %ld %ld %ld", mParam.lSkinSoftenLevel, mParam.lSkinBrightLevel,
			mParam.lSlenderFaceLevel, mParam.lEyeEnlargmentLevel, mParam.lEyeBrightLevel,
			mParam.lTeethWhiteLevel, mParam.lRemoveShineLevel, mParam.lSkinRuddyLevel);
	}

	mFaceBeautyLevel = param.faceBeautyLevel;

	return res;
}

int32 
ArcSoftFaceBeauty::processFaceBeauty(LPASVLOFFSCREEN srcImg, GNDataType dataType)
{
	MRESULT res = 0;
	MLong nFaceNum = 0;
	AgeGenderResult_t ageGenderResult = {0};
	PAGE_GENDER_INFO* pAgeGenderInfo = NULL;

	memset(&ageGenderResult, 0, sizeof(AgeGenderResult_t));

	if (mArcSoftFaceProcess == NULL) {
		PRINTE("%s mArcSoftFaceProcess is NULL", __func__);
		return res;
	}
	
	if (!mArcSoftFaceProcess->isFaceProcessEnable()) {
		PRINTE("Haven't initialized the facebeauty enginer.");
		return res;
	}

	res = mArcSoftFaceProcess->faceDetection(srcImg, &pAgeGenderInfo, &nFaceNum, dataType);
	if (res != 0) {
		PRINTD("%s Failed to detect face [#%ld]", __func__, res);
		return res;
	}

	ageGenderResult.faceNumber = nFaceNum;

	for (int i = 0; i < nFaceNum; i ++) {
		BEAUTY_PARAM curParam = {0};

		ageGenderResult.ageResult[i] = pAgeGenderInfo[i]->age;
		ageGenderResult.genderResult[i] = pAgeGenderInfo[i]->gender;
		ageGenderResult.faceRect[i].bottom = pAgeGenderInfo[i]->rtFace.bottom;
		ageGenderResult.faceRect[i].left = pAgeGenderInfo[i]->rtFace.left;
		ageGenderResult.faceRect[i].right = pAgeGenderInfo[i]->rtFace.right;
		ageGenderResult.faceRect[i].top = pAgeGenderInfo[i]->rtFace.top;

		//set beauty param before process
		updateCurBeautyParam(&curParam, pAgeGenderInfo[i], &ageGenderResult.flawlessLevel[i]);
		//PRINTD("%s ageGenderArray %d, age %d gender %d", __func__, i ,ageGenderResult.ageResult[i],
		//	ageGenderResult.genderResult[i]);
		
		res = mArcSoftFaceProcess->processFaceBeauty(srcImg, &curParam, i, dataType);
		if (res != 0) {
			PRINTD("%s Face index[%d], process failed [#ld]", __func__, i ,res);
		}
	}

	if (mListener != NULL && nFaceNum > 0) {
		mListener->notify(GN_CAMERA_MSG_TYPE_AGEGENDER_DETECTION, 0, 0, 0, (void*)&ageGenderResult);
	}

	return res;
}

int32 
ArcSoftFaceBeauty::enableFaceBeauty()
{
	MRESULT res = 0; 
	
	if (mArcSoftFaceProcess != NULL) {
		mArcSoftFaceProcess->enableFaceProcess(GN_CAMERA_FACE_FEATURE_FACEBEAUTY);
	}

	return res;
}

int32 
ArcSoftFaceBeauty::disableFaceBeauty()
{
	MRESULT res = 0; 

	if (mArcSoftFaceProcess != NULL) {
		mArcSoftFaceProcess->disableFaceProcess(GN_CAMERA_FACE_FEATURE_FACEBEAUTY);
	}

	return res;
}

void
ArcSoftFaceBeauty::updateCurBeautyParam(BEAUTY_PARAM* param, PAGE_GENDER_INFO ageGenderInfo, int* beautyLevel)
{
	int flawlessLevel = 0;
	
	if (mFaceBeautyLevel == 0) {
		//set specific beauty param accroding to the age and gender.
		flawlessLevel = calculateFlawlessBeautyLevel(ageGenderInfo->age, ageGenderInfo->gender);
		*param = mFBParamArray[flawlessLevel - 1];		
	} else {
		*param = mParam;
	}

	*beautyLevel = flawlessLevel;
}

};
