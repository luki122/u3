/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for face beauty.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-09-07
 *
 *************************************************************************************/
 
#ifndef ANDROID_ARCSOFT_FACE_BEAUTY_H
#define ANDROID_ARCSOFT_FACE_BEAUTY_H

#include "abstypes.h"
#include "beauty_shot.h"

#include "asvloffscreen.h"
#include "ammem.h"
#include "merror.h"

#include "GNCameraFeatureDefs.h"
#include "GNCameraFeatureListener.h"
#include "GNFaceBeautyDefs.h"
#include "ArcSoftFaceProcess.h"

namespace android { 
class ArcSoftFaceBeauty {
public:
    ArcSoftFaceBeauty();
	~ArcSoftFaceBeauty();
	
	int32 init();
    void  deinit();
	int32 setCameraListener(GNCameraFeatureListener* listener);
	int32 setFaceBeautyParam(FaceBeautyParam const faceBeautyParam);
	int32 processFaceBeauty(LPASVLOFFSCREEN param, GNDataType dataType);
	int32 enableFaceBeauty();
	int32 disableFaceBeauty();

private:
	void updateCurBeautyParam(BEAUTY_PARAM* param, PAGE_GENDER_INFO ageGenderInfo, int* beautyLevel);
	int32 getFlawlessBeautyLevel(MLong age, MLong gender);

private:
	BEAUTY_PARAM mParam;
	bool mInitialized;
	GNCameraFeatureListener* mListener;
	ArcSoftFaceProcess* mArcSoftFaceProcess;
	int mFaceBeautyLevel;
};
};
#endif /* ANDROID_ARCSOFT_FACE_BEAUTY_H */
