#ifndef ANDROID_AUDIO_RESAMPLER_32BITS_H
#define ANDROID_AUDIO_RESAMPLER_32BITS_H

#include <stdint.h>
#include <sys/types.h>
#include <cutils/log.h>

#include "AudioResampler.h"

namespace android {

class AudioResampler32bits : public AudioResampler {
public:
 AudioResampler32bits(int bitDepth, int inChannelCount, int32_t sampleRate):
    filter_size(32), phase_shift(14), filter_shift(30),mFilter(0),
		inBufferR(0), inBufferL(0),mIndex(0),mFrac(0),
		mOutL(0),mOutR(0),mconsumed(0),
        AudioResampler(bitDepth, inChannelCount, sampleRate, MED_QUALITY) {
		phase_count = 1<<phase_shift;
		scale = 1 << filter_shift;
    }

 ~AudioResampler32bits(){
	if(!mFilter && mInSampleRate != 44100){
		delete mFilter;
		mFilter = 0;
	}

	if(!inBufferR){
		delete inBufferR;
		inBufferR = 0;
	}

	if(!inBufferL){
		delete inBufferL;
		inBufferL = 0;
	}
	
	if(!mOutL){
		delete mOutL;
		mOutL = 0;
	}

	if(!mOutR){
		delete mOutR;
		mOutR = 0;
	}
 }
    virtual void resample(int32_t* out, size_t outFrameCount,
            AudioBufferProvider* provider);
	void setSampleRate(int32_t inSampleRate);
    int64_t calculateInputPTS(int outputFrameIndex);
private:
	void init();
    void reset ();
    void resampleMono32(int32_t* out, size_t outFrameCount,
            AudioBufferProvider* provider);
    void resampleStereo32(int32_t* out, size_t outFrameCount,
            AudioBufferProvider* provider);
	int32_t av_clipl_int32_c(int64_t a);
	double bessel(double x);
	int32_t av_reduce(int &dst_num, int &dst_den,
					  int64_t num, int64_t den, int64_t max);
	int build_filter();
	int resampleInt32Stereo(int32_t *dest, const int32_t *sourceL,
					  const int32_t *sourceR, int n);
	int resampleInt32Mono(int32_t *dest, const int32_t *sourceL,
						  int n);
	int32_t *mFilter;
	size_t minFrameCount;
	double factor;
	int32_t filter_size;
	int32_t tap_count;
	int32_t alloc;
	int32_t phase_count;
	int32_t phase_shift;
	int32_t filter_shift;
	int32_t scale;
	int32_t filter_alloc;
	int32_t *inBufferR;
	int32_t *inBufferL;
	int32_t *mOutL;
	int32_t *mOutR;
	int ideal_dst_incr;
    int dst_incr;
    int dst_incr_div;
    int dst_incr_mod;
    int src_incr;
	int mIndex;
	int mFrac;
	int mconsumed;
};

}; // namespace android

#endif
