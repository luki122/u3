ifeq ($(strip $(BOARD_ENABLE_ECHONODE)),true)
LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        echonode.cpp


LOCAL_MODULE:= echonode
LOCAL_MODULE_TAGS := optional 

PRODUCT_COPY_FILES += $(LOCAL_PATH)/echonode.cfg:system/etc/echonode.cfg
include $(BUILD_EXECUTABLE) 
 
endif
