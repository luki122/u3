package com.android.internal.app;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.AuroraClassUtils;
import android.view.Window;

public class AuroraDialogClassHelper extends AuroraClassUtils{
	
	private static final String ALERT_CONTROLLER_CLASS_NAME="com.android.internal.app.AuroraAlertController";
	private static final String ALERT_PARAMS_CLASS_NAME="com.android.internal.app.AuroraAlertController$AuroraAlertParams";
	private Context mContext;
	private Window mWindow;
	public AuroraDialogClassHelper(Context context, DialogInterface di, Window window){
		this.mContext = context;
		this.mWindow = window;
		getTarget(ALERT_CONTROLLER_CLASS_NAME, 
				new Object[]{context,di,window}, Context.class,DialogInterface.class,Window.class);
	}
	
	

	public static class AuroraDialogAlertParamsHelper extends AuroraClassUtils {
		private Context context;
		public AuroraDialogAlertParamsHelper(Context context) {
			this.context = context;
			getTarget(ALERT_PARAMS_CLASS_NAME,new Object[] { context }, Context.class);
		}

	}


}
