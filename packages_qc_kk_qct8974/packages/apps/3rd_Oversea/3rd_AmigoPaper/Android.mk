ifeq ("$(APK_AMIGOPAPER_SUPPORT)","yes")
LOCAL_PATH := $(call my-dir)
$(info $(LOCAL_PATH))
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/AmigoPaper.apk:system/app/AmigoPaper.apk \
    $(LOCAL_PATH)/Vlife.apk:system/app/Vlife.apk \
    $(LOCAL_PATH)/lib/armeabi/libgraphic_gause_trans.so:system/lib/libgraphic_gause_trans.so
endif
