ifeq ("$(APK_GN_TELEPATH_SUPPORT)","yes")
LOCAL_PATH := $(call my-dir)

###############################################################################
# GMS Mandatory Apps (not published in Play Store)
# This binary is required for any Google application to work.
# It MUST be installed on all devices.
###############################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := GN_Telepath
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRIVILEGED_MODULE := true
LOCAL_CERTIFICATE := platform
include $(BUILD_PREBUILT)
endif
