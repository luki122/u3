ifeq ("$(APK_TOUCHPAL_SUPPORT)","yes")
LOCAL_PATH := $(call my-dir)

APK_NAME := TouchPal_Global_v5.5.0.4.113591

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/$(APK_NAME).apk:system/app/$(APK_NAME).apk 

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/LANGUAGE_PACK/BengaliPack.apk:system/app/BengaliPack.apk \
    $(LOCAL_PATH)/LANGUAGE_PACK/GujaratiPack.apk:system/app/GujaratiPack.apk \
    $(LOCAL_PATH)/LANGUAGE_PACK/HindiPack.apk:system/app/HindiPack.apk \
    $(LOCAL_PATH)/LANGUAGE_PACK/HinglishPack.apk:system/app/HinglishPack.apk \
    $(LOCAL_PATH)/LANGUAGE_PACK/KannadaPack.apk:system/app/KannadaPack.apk \
    $(LOCAL_PATH)/LANGUAGE_PACK/MalayalamPack.apk:system/app/MalayalamPack.apk \
    $(LOCAL_PATH)/LANGUAGE_PACK/MarathiPack.apk:system/app/MarathiPack.apk \
    $(LOCAL_PATH)/LANGUAGE_PACK/PunjabiPack.apk:system/app/PunjabiPack.apk \
    $(LOCAL_PATH)/LANGUAGE_PACK/TamilPack.apk:system/app/TamilPack.apk \
    $(LOCAL_PATH)/LANGUAGE_PACK/TeluguPack.apk:system/app/TeluguPack.apk \
    $(LOCAL_PATH)/LANGUAGE_PACK/UrduPack.apk:system/app/UrduPack.apk
endif
