
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)



LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := Twitter-com.twitter.android-4030809-v5.49.0
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := platform
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

ifeq ("$(APK_TWITTER_SUPPORT)","yes")
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/$(LOCAL_SRC_FILES):system/app/$(LOCAL_SRC_FILES) \
    $(LOCAL_PATH)/libtwittermedia.so:system/lib/libtwittermedia.so

include $(BUILD_PREBUILT)
endif

ifeq ("$(APK_TWITTER_SUPPORT)","yes_mtk")
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/$(LOCAL_SRC_FILES):system/vendor/operator/app/$(LOCAL_SRC_FILES)  \
    $(LOCAL_PATH)/libtwittermedia.so:system/vendor/lib/libtwittermedia.so

include $(BUILD_PREBUILT)
endif

ifeq ("$(APK_TWITTER_SUPPORT)","yes_data")
#modify by heling at 20140623 begin
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/$(LOCAL_SRC_FILES):data/app/$(LOCAL_SRC_FILES) \
    $(LOCAL_PATH)/$(LOCAL_SRC_FILES):system/gn_app_bk/$(LOCAL_SRC_FILES) 
#modify by heling at 20140623 end
include $(BUILD_PREBUILT)
endif

#Gionee lijinfang 2015-3-22 modify for CR01456635 begin
ifeq ("$(APK_TWITTER_SUPPORT)","yes_tcard")
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/$(LOCAL_SRC_FILES):tcard/system/gn_app_bk/$(LOCAL_MODULE)/$(LOCAL_SRC_FILES) 

include $(BUILD_PREBUILT)
endif
#Gionee lijinfang 2015-3-22 modify for CR01456635 end
