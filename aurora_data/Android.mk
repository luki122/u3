# Audio Package 4
# Include this file in a product makefile to include these audio files
# This is a larger package of sounds than the 1.0 release for devices
# that have larger internal flash.
LOCAL_PATH:= aurora_data

#add by hujianwei 20140918 
define all_files_under_subdir
$(foreach files, $(notdir $(wildcard $1)),$(dir $(1))$(files):$(2)/$(files)) 
endef

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/aurora-data.sh,system/etc) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/iuni/channel.xml,system/iuni) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/*.so,system/lib) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/etc/aurora/launcher/config/default_workspace.xml,system/iuni/aurora/launcher/config)


$(info $(GN_PROJECT))
E6=$(findstring WBW588502,$(GN_PROJECT))
U2=$(findstring NBW8902A01_A,$(GN_PROJECT))

ifeq ("$(U2)", "")
U2=$(findstring NBW8902A_32G_A,$(GN_PROJECT))
endif

U3=$(findstring NBL8910A01,$(GN_PROJECT))
U3m=$(findstring NBL8905A01,$(GN_PROJECT))
U2Sea=$(findstring NBW8902A_16G_A,$(GN_PROJECT))

ifeq ("$(U2Sea)", "")
U2Sea=$(findstring NBW8902A_32G_B,$(GN_PROJECT))
endif

ifneq ("$(U2)", "")

$(info "U2 application")

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/*.apk,system/app) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/lib/armeabi-v7a/*.so,system/lib) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/app/LBESecV3_IUNIOS.apk,system/app) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/lib/armeabi-v7a/lbesec,system/bin)

#add by luofu ,start
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/iuni/u2/rcconfig.xml,system/iuni) 
#add by luofu,end

#modify by liugj start
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/desktop/*.jpg,system/iuni/aurora/change/desktop) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/City/*.png,system/iuni/aurora/change/lockscreen/City) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/Color/*.png,system/iuni/aurora/change/lockscreen/Color) \
	$(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/Dream/*.png,system/iuni/aurora/change/lockscreen/Dream) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/etc/aurora/change/lockscreen/Timely/*.png,system/iuni/aurora/change/lockscreen/Timely)
#add by shigq start
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/*.xml,system/iuni/aurora/change/lockscreen) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/City/*.xml,system/iuni/aurora/change/lockscreen/City) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/Color/*.xml,system/iuni/aurora/change/lockscreen/Color) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/Dream/*.xml,system/iuni/aurora/change/lockscreen/Dream) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/etc/aurora/change/lockscreen/Timely/*.xml,system/iuni/aurora/change/lockscreen/Timely)
#add by shigq end
#modify by liugj end

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/aurora-data.sh,system/bin) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/iuni/bootanimation.zip,system/iuni) \

#modify by hujianwei start
### U2 audio
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/alarms/*.ogg,system/media/audio/alarms) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/notifications/*.ogg,system/media/audio/notifications) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/ringtones/*.ogg,system/media/audio/ringtones) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/ui/*.ogg,system/media/audio/ui)
#modify by hujianwei end

###sogou
PRODUCT_COPY_FILES += \
	$(call all_files_under_subdir,$(LOCAL_PATH)/system/app/lib/armeabi/*.so,system/lib)
#add by shigq start
### for NextDay
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/iuni/.lockscreen/NextDay/*.jpg,system/sdcard/iuni/.lockscreen/NextDay) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/iuni/.lockscreen/NextDay/*.xml,system/sdcard/iuni/.lockscreen/NextDay)
#add by shigq end
endif

ifneq ("$(E6)", "")                
$(info "E6 application")

#add by luofu ,start
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/iuni/rcconfig.xml,system/iuni) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/app/LBESecV3_IUNIOS.apk,system/app) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/lib/armeabi-v7a/lbesec,system/bin)
#add by luofu,end

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/iuni/bootanimation.zip,system/iuni) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/E6/*.apk,system/app)  \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/SogouInput_iuni.apk,system/app)  \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/lib/armeabi-v7a/lbesec,system/bin)
#modify by liugj start
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/desktop/E6/*.jpg,system/iuni/aurora/change/desktop) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/City/*.png,system/iuni/aurora/change/lockscreen/City) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/Color/*.png,system/iuni/aurora/change/lockscreen/Color) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/Dream/*.png,system/iuni/aurora/change/lockscreen/Dream) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/etc/aurora/change/lockscreen/Timely/*.png,system/iuni/aurora/change/lockscreen/Timely) 
#add by shigq start
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/*.xml,system/iuni/aurora/change/lockscreen) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/City/*.xml,system/iuni/aurora/change/lockscreen/City) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/Color/*.xml,system/iuni/aurora/change/lockscreen/Color) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/Dream/*.xml,system/iuni/aurora/change/lockscreen/Dream) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/etc/aurora/change/lockscreen/Timely/*.xml,system/iuni/aurora/change/lockscreen/Timely)
#add by shigq end
#modify by liugj end

#modify by hujianwei start
###E6 audio
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/E6/alarms/*.ogg,system/media/audio/alarms) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/E6/notifications/*.ogg,system/media/audio/notifications) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/E6/ringtones/*.ogg,system/media/audio/ringtones) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/E6/ui/*.ogg,system/media/audio/ui)
#modify by hujianwei end

###sogou
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/lib/armeabi/*.so,system/lib)
#add by shigq start
### for NextDay
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/iuni/.lockscreen/NextDay/*.jpg,system/sdcard/iuni/.lockscreen/NextDay) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/iuni/.lockscreen/NextDay/*.xml,system/sdcard/iuni/.lockscreen/NextDay)
#add by shigq end
endif

ifneq ("$(U2Sea)", "")
$(info "U2Sea application")

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U2Sea/*.apk,system/app) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U2Sea/priv-app/*.apk,system/priv-app) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/GnHotlinesProvider.apk,system/app) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U2Sea/lib/armeabi-v7a/*.so,system/lib) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U2Sea/lib/armeabi/*.so,system/lib) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/etc/init.rc,system/etc) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U2Sea/aurora-data.sh,system/bin) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U2Sea/systemfile_current_md5.sh,system/etc) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U2Sea/systemfile_original_md5.sh,system/etc) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U2Sea/systemfile_md5_check.sh,system/etc) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U2Sea/systemfile_md5_update.sh,system/etc) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/app/LBESecV3_IUNIOS.apk,system/app) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U2Sea/lib/armeabi-v7a/lbesec,system/bin)

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/desktop/U2Sea/*.jpg,system/iuni/aurora/change/desktop) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U2Sea/City/*.png,system/iuni/aurora/change/lockscreen/City) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U2Sea/Color/*.png,system/iuni/aurora/change/lockscreen/Color) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U2Sea/Dream/*.png,system/iuni/aurora/change/lockscreen/Dream) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/etc/aurora/change/lockscreen/Timely/*.png,system/iuni/aurora/change/lockscreen/Timely)

#add by shigq start
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U2Sea/*.xml,system/iuni/aurora/change/lockscreen) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U2Sea/City/*.xml,system/iuni/aurora/change/lockscreen/City) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U2Sea/Color/*.xml,system/iuni/aurora/change/lockscreen/Color) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U2Sea/Dream/*.xml,system/iuni/aurora/change/lockscreen/Dream) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/etc/aurora/change/lockscreen/Timely/*.xml,system/iuni/aurora/change/lockscreen/Timely)
#add by shigq end
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/iuni/U2Sea/rcconfig.xml,system/iuni) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/iuni/bootanimation.zip,system/iuni)

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/U2Sea/alarms/*.ogg ,system/media/audio/alarms) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/U2Sea/notifications/*.ogg,system/media/audio/notifications) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/U2Sea/ringtones/*.ogg,system/media/audio/ringtones) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/U2Sea/ui/*.ogg,system/media/audio/ui)
#add by shigq start
### for NextDay
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/iuni/.lockscreen/NextDay/*.jpg,system/sdcard/iuni/.lockscreen/NextDay) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/iuni/.lockscreen/NextDay/*.xml,system/sdcard/iuni/.lockscreen/NextDay)
#add by shigq end
endif


ifneq ("$(U3)", "")
$(info "U3 application")

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U3/*.apk,system/app) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U3/priv-app/*.apk,system/priv-app) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/GnHotlinesProvider.apk,system/app) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U3/lib/armeabi-v7a/*.so,system/lib) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U3/lib/armeabi/*.so,system/lib) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/etc/init.rc,system/etc) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U3/aurora-data.sh,system/bin)

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/desktop/U3/*.jpg,system/iuni/aurora/change/desktop) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3/City/*.png,system/iuni/aurora/change/lockscreen/City) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3/Color/*.png,system/iuni/aurora/change/lockscreen/Color) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3/Dream/*.png,system/iuni/aurora/change/lockscreen/Dream) \
	$(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3/Green/*.png,system/iuni/aurora/change/lockscreen/Green) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3/Timely/*.png,system/iuni/aurora/change/lockscreen/Timely) 
#add by shigq start
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3/*.xml,system/iuni/aurora/change/lockscreen) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3/City/*.xml,system/iuni/aurora/change/lockscreen/City) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3/Color/*.xml,system/iuni/aurora/change/lockscreen/Color) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3/Dream/*.xml,system/iuni/aurora/change/lockscreen/Dream) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3/Green/*.xml,system/iuni/aurora/change/lockscreen/Green) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3/Timely/*.xml,system/iuni/aurora/change/lockscreen/Timely)
#add by shigq end

#add by luofu ,start
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/iuni/u3/rcconfig.xml,system/iuni) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/iuni/u3/bootanimation.zip,system/iuni)
#add by luofu,end

#modify by hujianwei start
###U3 audio
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/U3/alarms/*.ogg ,system/media/audio/alarms) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/U3/notifications/*.ogg,system/media/audio/notifications) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/U3/ringtones/*.ogg,system/media/audio/ringtones) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/U3/ui/*.ogg,system/media/audio/ui)
#modify by hujianwei end


#add by hujianwei 2014-08-11
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/DCIM/U3/*.png,system/sdcard/DCIM) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/DCIM/U3/*.jpg,system/sdcard/DCIM)


PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U3/systemfile_current_md5.sh,system/etc) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U3/systemfile_original_md5.sh,system/etc) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U3/systemfile_md5_check.sh,system/etc) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U3/systemfile_md5_update.sh,system/etc)

#add by shigq start
### for NextDay
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/iuni/.lockscreen/NextDay/*.jpg,system/sdcard/iuni/.lockscreen/NextDay) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/iuni/.lockscreen/NextDay/*.xml,system/sdcard/iuni/.lockscreen/NextDay)
#add by shigq end

PRODUCT_COPY_FILES += \
       $(call all_files_under_subdir,$(LOCAL_PATH)/app/U3/Aurora_Store.apk,system/userapp)


endif


ifneq ("$(U3m)", "")
$(info "U3m application")

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U3m/*.apk,system/app) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U3m/priv-app/*.apk,system/priv-app) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/GnHotlinesProvider.apk,system/app) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U3m/lib/armeabi-v7a/*.so,system/lib) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U3m/lib/armeabi/*.so,system/lib) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/etc/init.rc,system/etc) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/U3m/aurora-data.sh,system/bin)


PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/Cat/*.png,system/iuni/aurora/change/lockscreen/Cat) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/Dream/*.png,system/iuni/aurora/change/lockscreen/Dream) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/Timely/*.png,system/iuni/aurora/change/lockscreen/Timely) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/City/*.png,system/iuni/aurora/change/lockscreen/City) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/Color/*.png,system/iuni/aurora/change/lockscreen/Color)
ifdef GN3RD_GMS_SUPPORT
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/desktop/U3m/*.jpg,system/iuni/aurora/change/desktop)
else
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/desktop/U3m/*.jpg,system/iuni/aurora/change/desktop)
endif

#add by shigq start
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/*.xml,system/iuni/aurora/change/lockscreen) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/Cat/*.xml,system/iuni/aurora/change/lockscreen/Cat) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/Dream/*.xml,system/iuni/aurora/change/lockscreen/Dream) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/Timely/*.xml,system/iuni/aurora/change/lockscreen/Timely) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/City/*.xml,system/iuni/aurora/change/lockscreen/City) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/Color/*.xml,system/iuni/aurora/change/lockscreen/Color)
#add by shigq end
#add by luofu ,start
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/iuni/u3m/rcconfig.xml,system/iuni) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/iuni/u3m/bootanimation.zip,system/iuni)
#add by luofu,end

#modify by hujianwei start
###U3 audio
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/U3m/alarms/*.ogg ,system/media/audio/alarms) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/U3m/notifications/*.ogg,system/media/audio/notifications) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/U3m/ringtones/*.ogg,system/media/audio/ringtones) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/media/audio/U3m/ui/*.ogg,system/media/audio/ui)
#modify by hujianwei end
#add by hujianwei 2014-08-11
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/DCIM/U3m/*.png,system/sdcard/DCIM) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/DCIM/U3m/*.jpg,system/sdcard/DCIM)


PRODUCT_COPY_FILES += \
       $(call all_files_under_subdir,$(LOCAL_PATH)/app/U3m/Aurora_Store.apk,system/userapp)
#add by shigq start
### for NextDay
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/iuni/.lockscreen/NextDay/*.jpg,system/sdcard/iuni/.lockscreen/NextDay) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/iuni/.lockscreen/NextDay/*.xml,system/sdcard/iuni/.lockscreen/NextDay)
#add by shigq end

#add by shigq start
#PRODUCT_COPY_FILES += \
#        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/Elegant/*.png,system/iuni/aurora/change/lockscreen/Elegant) \
#        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/Elegant/*.xml,system/iuni/aurora/change/lockscreen/Elegant)
#add by shigq end

#add by shigq start
#for India
#PRODUCT_COPY_FILES += \
#        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/City/*.png,system/iuni/aurora/change/lockscreen/City) \
#        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/City/*.xml,system/iuni/aurora/change/lockscreen/City) \
#        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/Dream/*.png,system/iuni/aurora/change/lockscreen/Dream) \
#        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/aurora/change/lockscreen/U3m/Dream/*.xml,system/iuni/aurora/change/lockscreen/Dream)
#add by shigq end

endif


PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/iuni/IUNIOS.png,system/iuni)

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/app/lib/armeabi-v7a/*.so,system/lib) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/system/framework/*.jar,system/framework)

ifndef GN3RD_GMS_SUPPORT
##add test MP3 and MP4
PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/mov/*.mp3,system/sdcard/music) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/mov/*.mp4,system/sdcard) \
        $(call all_files_under_subdir,$(LOCAL_PATH)/sdcard/lrc/*.lrc,system/sdcard/lyric)
endif
 
###wakeup list
PRODUCT_COPY_FILES += \
       $(call all_files_under_subdir,$(LOCAL_PATH)/system/etc/wakeup_apk_list,system/etc)

###gallery3d filter maskfile, added by shenqianfeng on 2014-09-28
PRODUCT_COPY_FILES += \
       $(call all_files_under_subdir,$(LOCAL_PATH)/system/iuni/com.android.gallery3d/maskfile/*.data,system/iuni/aurora/gallery/maskfile)

