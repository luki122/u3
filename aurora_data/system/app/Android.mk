ifeq ("$(GN_APK_AURORA_CAMERA_SUPPORT)","yes")
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := Aurora_Camera
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

PRODUCT_COPY_FILES += $(foreach pathfile, $(shell find $(LOCAL_PATH) -name "*.so"), $(pathfile):system/lib/$(notdir $(pathfile))) #skip

include $(BUILD_PREBUILT)
endif
