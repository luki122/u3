#!/bin/bash
sh /system/etc/systemfile_current_md5.sh

tmp_current=`md5 /data/aurora/system_original_md5.file`
currentfile_md5=${tmp_current:0:32}

tmp_original=`md5 /data/aurora/system_current_md5.file`
originalfile_md5=${tmp_original:0:32}

echo "orginal_system_md5:"${originalfile_md5}
echo "current_system_md5:"${currentfile_md5}
if [[ ${currentfile_md5} != ${originalfile_md5} ]];then
    echo "系统已被修改."
    exit 1
else
    echo "系统未被修改."
    exit 0
fi

